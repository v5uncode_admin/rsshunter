package us.congcong.rsshunter.service;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.jsoup.nodes.Element;
import org.springframework.stereotype.Service;

import us.congcong.rsshunter.model.SampleChannel;
import us.congcong.rsshunter.model.SampleContent;

@Service
public class WeiboRssService {
//	public static void main(String[] args) {
//		WeiboRssService wr = new WeiboRssService();
//		wr.produceRssByUid(1671353227l);
//	}

	public Map<String, Object> produceRssByUid(String uid) {
		Map<String, Object> info = new HashMap<String,Object>();
		
		SampleChannel channel = new SampleChannel();
		List<SampleContent> list = new ArrayList<SampleContent>();
		
		Document doc = null;
		try {
			String url = "http://service.weibo.com/widget/widget_blog.php?uid="+uid;
			doc = Jsoup.connect(url).get();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if(doc != null){
			channel.setTitle(doc.title());
			channel.setDescription(doc.title() + " - 通过Rss Hunter获取【https://gitee.com/accacc/rsshunter】");
			channel.setLink("");
			
			Elements items = doc.select(".wgtCell_con");
			
			for (Element item : items) {
				SampleContent content = new SampleContent();
				String itemContent = item.select(".wgtCell_txt").html();
				content.setSummary(itemContent);
				
				String itemTitle = item.select(".wgtCell_txt").text();
				content.setTitle(itemTitle);
				
				String itemTime = item.select(".wgtCell_txtBot .wgtCell_tm a").text();
				//TODO 处理时间
				content.setCreatedDate(null);
				
				String itemUrl = item.select(".wgtCell_txtBot .wgtCell_tm a").attr("href");
				content.setUrl(itemUrl);
				
				list.add(content);
			}
		} else {
			return null;
		}
		info.put("channel", channel);
		info.put("items", list);
		return info;
	}

}
